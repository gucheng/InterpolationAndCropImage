import pygame,sys
import pytweening
from transitions import Machine
# Game options/settings
screenWidth = 600
screenHeight = 600
title = "Interpolation"
FPS = 60

# DefineColors
black = (0, 0, 0)
blue = (0, 0, 255)


class Game:
	states=["LeftMoving","RightMoving"]
	def __init__(self):
        # initialize pygame and create window
		pygame.init()
		self.screen = pygame.display.set_mode((screenWidth, screenHeight))
		pygame.display.set_caption(title)
		self.clock = pygame.time.Clock()
		self.A=30
		self.B=570
		self.p=0
		self.xPos=300
		self.speed=5
		
		self.machine = Machine(model=self, states=Game.states, initial='RightMoving')
		self.machine.add_transition(trigger='MoveLeft', source='RightMoving', dest='LeftMoving')
		self.machine.add_transition(trigger='MoveRight', source='LeftMoving', dest='RightMoving')



	def update(self):
		self.clock.tick(FPS)
		self.events()
		print(self.xPos);
		self.screen.fill(black)
		
		if(self.state=="RightMoving"):
			self.xPos+=pytweening.easeOutSine(self.speed)*100
			if(self.xPos>=500):
				self.MoveLeft()
		if(self.state=="LeftMoving"):
			self.xPos-=pytweening.easeOutElastic(self.speed)*100
			if(self.xPos<=100):
				self.MoveRight()
		
		pygame.draw.circle(self.screen,blue,(int(self.xPos),300),30)
		pygame.display.update()


	
	def events(self):
            # Process input(events)
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				pygame.quit()
				sys.exit()

game=Game()
while True:
	game.update()


